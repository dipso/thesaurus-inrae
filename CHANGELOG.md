#V2.2 (11/12/2024)
**Chiffres globaux**
- nombre de concepts : 	16 186
- nombre de termes FR : 22 852 / EN : 19 306
- nombre de concepts avec au moins un terme anglais :  14 714 (91%)
- nombre de définitions : 982
- nombre d'alignements : 3166 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 05/07/2024)**
- nombre de définitions ajoutées : 74
- nombre de concepts créés : 19
- Concepts dépréciés : 
http://opendata.inrae.fr/thesaurusINRAE/c_11454
http://opendata.inrae.fr/thesaurusINRAE/c_1161
http://opendata.inrae.fr/thesaurusINRAE/c_12551
http://opendata.inrae.fr/thesaurusINRAE/c_1270
http://opendata.inrae.fr/thesaurusINRAE/c_13321
http://opendata.inrae.fr/thesaurusINRAE/c_13361
http://opendata.inrae.fr/thesaurusINRAE/c_14156
http://opendata.inrae.fr/thesaurusINRAE/c_15513
http://opendata.inrae.fr/thesaurusINRAE/c_16278
http://opendata.inrae.fr/thesaurusINRAE/c_17393
http://opendata.inrae.fr/thesaurusINRAE/c_16847
http://opendata.inrae.fr/thesaurusINRAE/c_17302
http://opendata.inrae.fr/thesaurusINRAE/c_17374
http://opendata.inrae.fr/thesaurusINRAE/c_6504
http://opendata.inrae.fr/thesaurusINRAE/c_8111
http://opendata.inrae.fr/thesaurusINRAE/c_21485
http://opendata.inrae.fr/thesaurusINRAE/c_22550
http://opendata.inrae.fr/thesaurusINRAE/c_2365
http://opendata.inrae.fr/thesaurusINRAE/c_415
http://opendata.inrae.fr/thesaurusINRAE/c_4212
http://opendata.inrae.fr/thesaurusINRAE/c_4784
http://opendata.inrae.fr/thesaurusINRAE/c_6271
http://opendata.inrae.fr/thesaurusINRAE/c_6713
http://opendata.inrae.fr/thesaurusINRAE/c_5705
http://opendata.inrae.fr/thesaurusINRAE/c_6312
http://opendata.inrae.fr/thesaurusINRAE/c_6845
http://opendata.inrae.fr/thesaurusINRAE/c_9811
http://opendata.inrae.fr/thesaurusINRAE/c_7dab230e
http://opendata.inrae.fr/thesaurusINRAE/c_8076
http://opendata.inrae.fr/thesaurusINRAE/c_8203
http://opendata.inrae.fr/thesaurusINRAE/c_8326
http://opendata.inrae.fr/thesaurusINRAE/c_8744
http://opendata.inrae.fr/thesaurusINRAE/c_9130
http://opendata.inrae.fr/thesaurusINRAE/c_9497
http://opendata.inrae.fr/thesaurusINRAE/c_e2b379b6


**Amélioration du contenu** 
Les "groupes" (skos:Collection) ont été renommées en "collections" par souci d'harmonisation. Leur nom a été modifié de "GR. SCIENCE OUVERTE" à "coll SCIENCE OUVERTE" par exemple. 
Ajout de concepts et de définitions dans le domaine de la métabolomique pour répondre aux besoin d'un projet scientifique.


# V2.1 (05-07-2024)
**Chiffres globaux**
- nombre de concepts : 16167	
- nombre de termes FR :   22 789 / EN :17 530  
- nombre de concepts avec au moins un terme anglais : 13 339 (82%)
- nombre de définitions : 908
- nombre d'alignements : 3166 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 05/02/2024)**
- nombre de définitions ajoutées : 28
- nombre de concepts créés : 50
- Concepts dépréciés : 

<http://opendata.inrae.fr/thesaurusINRAE/c_14156>
<http://opendata.inrae.fr/thesaurusINRAE/c_8147>
<http://opendata.inrae.fr/thesaurusINRAE/c_14427>
<http://opendata.inrae.fr/thesaurusINRAE/c_14605>
<http://opendata.inrae.fr/thesaurusINRAE/c_14725>
<http://opendata.inrae.fr/thesaurusINRAE/c_14740>
<http://opendata.inrae.fr/thesaurusINRAE/c_15220>
<http://opendata.inrae.fr/thesaurusINRAE/c_15229>
<http://opendata.inrae.fr/thesaurusINRAE/c_1530>
<http://opendata.inrae.fr/thesaurusINRAE/c_15407>
<http://opendata.inrae.fr/thesaurusINRAE/c_15554>
<http://opendata.inrae.fr/thesaurusINRAE/c_15881>
<http://opendata.inrae.fr/thesaurusINRAE/c_17378>
<http://opendata.inrae.fr/thesaurusINRAE/c_16189>
<http://opendata.inrae.fr/thesaurusINRAE/c_16278>
<http://opendata.inrae.fr/thesaurusINRAE/c_16847>
<http://opendata.inrae.fr/thesaurusINRAE/c_17203>
<http://opendata.inrae.fr/thesaurusINRAE/c_17313>
<http://opendata.inrae.fr/thesaurusINRAE/c_17377>
<http://opendata.inrae.fr/thesaurusINRAE/c_17492>
<http://opendata.inrae.fr/thesaurusINRAE/c_17724>
<http://opendata.inrae.fr/thesaurusINRAE/c_18355>
<http://opendata.inrae.fr/thesaurusINRAE/c_19580>
<http://opendata.inrae.fr/thesaurusINRAE/c_20185>
<http://opendata.inrae.fr/thesaurusINRAE/c_22550>
<http://opendata.inrae.fr/thesaurusINRAE/c_26272>
<http://opendata.inrae.fr/thesaurusINRAE/c_25555>
<http://opendata.inrae.fr/thesaurusINRAE/c_2816>
<http://opendata.inrae.fr/thesaurusINRAE/c_5359a1c2>
<http://opendata.inrae.fr/thesaurusINRAE/c_4121>
<http://opendata.inrae.fr/thesaurusINRAE/c_5712>
<http://opendata.inrae.fr/thesaurusINRAE/c_5716>
<http://opendata.inrae.fr/thesaurusINRAE/c_6046>
<http://opendata.inrae.fr/thesaurusINRAE/c_6108>
<http://opendata.inrae.fr/thesaurusINRAE/c_9227>
<http://opendata.inrae.fr/thesaurusINRAE/c_6876>
<http://opendata.inrae.fr/thesaurusINRAE/c_7087>
<http://opendata.inrae.fr/thesaurusINRAE/c_7223>
<http://opendata.inrae.fr/thesaurusINRAE/c_7506>
<http://opendata.inrae.fr/thesaurusINRAE/c_8076>
<http://opendata.inrae.fr/thesaurusINRAE/c_8326>
<http://opendata.inrae.fr/thesaurusINRAE/c_8532>
<http://opendata.inrae.fr/thesaurusINRAE/c_8606>
<http://opendata.inrae.fr/thesaurusINRAE/c_9722>
<http://opendata.inrae.fr/thesaurusINRAE/c_9831>
<http://opendata.inrae.fr/thesaurusINRAE/c_c92d9122>
<http://opendata.inrae.fr/thesaurusINRAE/c_18863eb3>
<http://opendata.inrae.fr/thesaurusINRAE/c_d7eb52d4>
<http://opendata.inrae.fr/thesaurusINRAE/c_ba789396>
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy

# V2.0 (05-02-2024)
**Chiffres globaux**
- nombre de concepts : 	16 114
- nombre de termes FR : 22 701  / EN : 17 053
- nombre de définitions : 880
- nombre d'alignements : 3166 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 30/01/2023)**
- nombre de défintions ajoutées : 73
- nombre de concepts créés : 117
- nombre de concepts dépréciés : 49

Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_1024>
<http://opendata.inrae.fr/thesaurusINRAE/c_11661>
<http://opendata.inrae.fr/thesaurusINRAE/c_11683>
<http://opendata.inrae.fr/thesaurusINRAE/c_11792>
<http://opendata.inrae.fr/thesaurusINRAE/c_12014>
<http://opendata.inrae.fr/thesaurusINRAE/c_1207>
<http://opendata.inrae.fr/thesaurusINRAE/c_12276>
<http://opendata.inrae.fr/thesaurusINRAE/c_12421>
<http://opendata.inrae.fr/thesaurusINRAE/c_13107>
<http://opendata.inrae.fr/thesaurusINRAE/c_13130>
<http://opendata.inrae.fr/thesaurusINRAE/c_13321>
<http://opendata.inrae.fr/thesaurusINRAE/c_13488>
<http://opendata.inrae.fr/thesaurusINRAE/c_13639>
<http://opendata.inrae.fr/thesaurusINRAE/c_14605>
<http://opendata.inrae.fr/thesaurusINRAE/c_14652>
<http://opendata.inrae.fr/thesaurusINRAE/c_14661>
<http://opendata.inrae.fr/thesaurusINRAE/c_14826>
<http://opendata.inrae.fr/thesaurusINRAE/c_14955>
<http://opendata.inrae.fr/thesaurusINRAE/c_15034>
<http://opendata.inrae.fr/thesaurusINRAE/c_15410>
<http://opendata.inrae.fr/thesaurusINRAE/c_16208>
<http://opendata.inrae.fr/thesaurusINRAE/c_16784>
<http://opendata.inrae.fr/thesaurusINRAE/c_21248>
<http://opendata.inrae.fr/thesaurusINRAE/c_21977>
<http://opendata.inrae.fr/thesaurusINRAE/c_21984>
<http://opendata.inrae.fr/thesaurusINRAE/c_24894>
<http://opendata.inrae.fr/thesaurusINRAE/c_2492>
<http://opendata.inrae.fr/thesaurusINRAE/c_378>
<http://opendata.inrae.fr/thesaurusINRAE/c_417>
<http://opendata.inrae.fr/thesaurusINRAE/c_4273>
<http://opendata.inrae.fr/thesaurusINRAE/c_6158>
<http://opendata.inrae.fr/thesaurusINRAE/c_6414>
<http://opendata.inrae.fr/thesaurusINRAE/c_7662>
<http://opendata.inrae.fr/thesaurusINRAE/c_780>
<http://opendata.inrae.fr/thesaurusINRAE/c_7974>
<http://opendata.inrae.fr/thesaurusINRAE/c_8049>
<http://opendata.inrae.fr/thesaurusINRAE/c_8056>
<http://opendata.inrae.fr/thesaurusINRAE/c_8145>
<http://opendata.inrae.fr/thesaurusINRAE/c_8183>
<http://opendata.inrae.fr/thesaurusINRAE/c_8541>
<http://opendata.inrae.fr/thesaurusINRAE/c_8875>
<http://opendata.inrae.fr/thesaurusINRAE/c_8970>
<http://opendata.inrae.fr/thesaurusINRAE/c_9074>
<http://opendata.inrae.fr/thesaurusINRAE/c_9101>
<http://opendata.inrae.fr/thesaurusINRAE/c_9164>
<http://opendata.inrae.fr/thesaurusINRAE/c_9384>
<http://opendata.inrae.fr/thesaurusINRAE/c_9687>
<http://opendata.inrae.fr/thesaurusINRAE/c_9695>


**Amélioration du contenu** 
- Restructuration du thésaurus afin de permettre une représentation plus classique de la hiérarchie. Création de conceptMT (skos:concept) correspondant à chaque microthésaurus (skos:conceptScheme). Chaque concept skos:topConceptOf d'un microthésaurus se retrouve donc skos:narrower du conceptMT correspondant.
- Fin de la régorganisation du domaine Chimie avec la suppresion du microthésaurus analyses chimiques et physico-chimiques.
- Ajout de traductions : 2356 concepts ont été complétés avec au moins un terme anglais, portant la couverture anglaise du thésaurus de 67% à 81%.
- Amélioration continue du thésaurus en réponse aux demandes des utilisateurs : ajout de concepts, de termes, de définitions, etc.


# V1.7 (25-09-2023)
**Chiffres globaux**
- nombre de concepts : 	15 997
- nombre de termes FR : 22 530  / EN : 13 706
- nombre de définitions : 807
- nombre d'alignements : 3147 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 30/01/2023)**
- nombre de défintions ajoutées : 31
- nombre de concepts créés : 26
- nombre de concepts dépréciés : 12
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_16472>
<http://opendata.inrae.fr/thesaurusINRAE/c_10862>
<http://opendata.inrae.fr/thesaurusINRAE/c_7473>
<http://opendata.inrae.fr/thesaurusINRAE/c_3480>
<http://opendata.inrae.fr/thesaurusINRAE/c_21296>
<http://opendata.inrae.fr/thesaurusINRAE/c_8326>
<http://opendata.inrae.fr/thesaurusINRAE/c_2396>
<http://opendata.inrae.fr/thesaurusINRAE/c_17607>
<http://opendata.inrae.fr/thesaurusINRAE/c_1153>
<http://opendata.inrae.fr/thesaurusINRAE/c_17752>
<http://opendata.inrae.fr/thesaurusINRAE/c_22727>
<http://opendata.inrae.fr/thesaurusINRAE/c_d6cc418f>

**Amélioration du contenu** 
- Amélioration continue du thésaurus en réponse aux demandes des utilisateurs : ajout de concepts, de termes, de définitions, etc.
- les concepts dépréciés ont été retirés des collections (Définis et Alignés)  
- Révision des règles de positionnement des concepts en lien avec l’énergie dans les domaines Environnement  et Physique:  les concepts qui sont des sources d'énergie d'origine naturelle et qui sont sous forme brute (ex.pétrole, gaz, charbon) sont sous ressource énergétique du MT Environnement et ressources naturelles. Les concepts plus génériques qui portent sur des formes d’énergie  (ex. combustible, carburant, énergie fossile, etc. ) sont placés en MT Energétique et thermodynamique. 


# V1.6 (30-06-2023)
**Chiffres globaux**
- nombre de concepts : 	15 971
- nombre de termes FR :  22 480 / EN : 13 652
- nombre de définitions : 776
- nombre d'alignements : 3169 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 30/01/2023)**
- nombre de défintions ajoutées : 17
- nombre de concepts créés : 128
- nombre de concepts dépréciés : 32
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_14359>
<http://opendata.inrae.fr/thesaurusINRAE/c_5953>
<http://opendata.inrae.fr/thesaurusINRAE/c_12565>
<http://opendata.inrae.fr/thesaurusINRAE/c_7539>
<http://opendata.inrae.fr/thesaurusINRAE/c_13505>
<http://opendata.inrae.fr/thesaurusINRAE/c_10445>
<http://opendata.inrae.fr/thesaurusINRAE/c_3480>
<http://opendata.inrae.fr/thesaurusINRAE/c_21471>
<http://opendata.inrae.fr/thesaurusINRAE/c_11430>
<http://opendata.inrae.fr/thesaurusINRAE/c_6249>
<http://opendata.inrae.fr/thesaurusINRAE/c_13117>
<http://opendata.inrae.fr/thesaurusINRAE/c_2919>
<http://opendata.inrae.fr/thesaurusINRAE/c_13400>
<http://opendata.inrae.fr/thesaurusINRAE/c_4957>
<http://opendata.inrae.fr/thesaurusINRAE/c_1194>
<http://opendata.inrae.fr/thesaurusINRAE/c_3933>
<http://opendata.inrae.fr/thesaurusINRAE/c_13528>
<http://opendata.inrae.fr/thesaurusINRAE/c_18187>
<http://opendata.inrae.fr/thesaurusINRAE/c_14443>
<http://opendata.inrae.fr/thesaurusINRAE/c_1211>
<http://opendata.inrae.fr/thesaurusINRAE/c_10032>
<http://opendata.inrae.fr/thesaurusINRAE/c_16543>
<http://opendata.inrae.fr/thesaurusINRAE/c_14203>
<http://opendata.inrae.fr/thesaurusINRAE/c_8250>
<http://opendata.inrae.fr/thesaurusINRAE/c_7006>
<http://opendata.inrae.fr/thesaurusINRAE/c_591>
<http://opendata.inrae.fr/thesaurusINRAE/c_1958>
<http://opendata.inrae.fr/thesaurusINRAE/c_7958>
<http://opendata.inrae.fr/thesaurusINRAE/c_14474>
<http://opendata.inrae.fr/thesaurusINRAE/c_15297>
<http://opendata.inrae.fr/thesaurusINRAE/c_16244>
<http://opendata.inrae.fr/thesaurusINRAE/c_10685>


**Evolution du périmètre des microthésaurus**
- Restructuration du domaine « Chimie » : le MT « Propriétés chimiques et physico-chimiques » est désormais déprécié. Les concepts ont été répartis dans les  MT « Science des matériaux et de la matière » et « Substances et produits chimiques ». 

**Amélioration du contenu**
-	Relecture du microthésaurus  (MT) « Neurosciences » : David Val-Laillet, chercheur à l’UMR Nutrition, métabolismes et cancer au centre Bretagne-Normandie, a apporté son expertise scientifique.	
-	Amélioration continue du thésaurus en réponse aux demandes des utilisateurs : ajout de concepts, de termes, de définitions, etc. 
-   Correction/Suppression d'alignements erronés vers Agrovoc 


# V1.5 (30-01-2023)
**Chiffres globaux**
- nombre de concepts : 	15 843
- nombre de termes FR :  22 317 / EN : 13 498
- nombre de définitions : 759
- nombre d'alignements : 3172 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu (depuis le 16/09/2022)**
- nombre de défintions ajoutées : 164
- nombre de concepts créés : 113
- nombre de concepts dépréciés : 33
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_20299>
<http://opendata.inrae.fr/thesaurusINRAE/c_22203>
<http://opendata.inrae.fr/thesaurusINRAE/c_20397>
<http://opendata.inrae.fr/thesaurusINRAE/c_5907>
<http://opendata.inrae.fr/thesaurusINRAE/c_3052>
<http://opendata.inrae.fr/thesaurusINRAE/c_15873>
<http://opendata.inrae.fr/thesaurusINRAE/c_2750>
<http://opendata.inrae.fr/thesaurusINRAE/c_22418>
<http://opendata.inrae.fr/thesaurusINRAE/c_4003>
<http://opendata.inrae.fr/thesaurusINRAE/c_20496>
<http://opendata.inrae.fr/thesaurusINRAE/c_4972>
<http://opendata.inrae.fr/thesaurusINRAE/c_22020>
<http://opendata.inrae.fr/thesaurusINRAE/c_7777>
<http://opendata.inrae.fr/thesaurusINRAE/c_9589>
<http://opendata.inrae.fr/thesaurusINRAE/c_2238>
<http://opendata.inrae.fr/thesaurusINRAE/c_202>
<http://opendata.inrae.fr/thesaurusINRAE/c_230>
<http://opendata.inrae.fr/thesaurusINRAE/c_6107>
<http://opendata.inrae.fr/thesaurusINRAE/c_11512>
<http://opendata.inrae.fr/thesaurusINRAE/c_13556>
<http://opendata.inrae.fr/thesaurusINRAE/c_16357>
<http://opendata.inrae.fr/thesaurusINRAE/c_22877>
<http://opendata.inrae.fr/thesaurusINRAE/c_15559>
<http://opendata.inrae.fr/thesaurusINRAE/c_21322>
<http://opendata.inrae.fr/thesaurusINRAE/c_191>
<http://opendata.inrae.fr/thesaurusINRAE/c_4966>
<http://opendata.inrae.fr/thesaurusINRAE/c_4006>
<http://opendata.inrae.fr/thesaurusINRAE/c_22442>
<http://opendata.inrae.fr/thesaurusINRAE/c_13451>
<http://opendata.inrae.fr/thesaurusINRAE/c_23047>
<http://opendata.inrae.fr/thesaurusINRAE/c_4453>
<http://opendata.inrae.fr/thesaurusINRAE/c_21334>
<http://opendata.inrae.fr/thesaurusINRAE/c_c2c97dd1>

**Evolution du périmètre des microthésaurus**

- Changement de nom du microthésaurus CHI Elements, composés et molécules chimiques --> CHI Substances et produits chimiques.
- Changement de nom du microthésaurus PHY Energie et thermodynamique --> PHY Energétique et thermodynamique.
- Révision du périmètre et changement de nom de 4 microthésaurus du domaine 09.PHYSIQUE : PHY Hydraulique et aéraulique --> PHY Mécanique des fluides et des solides, rhéologie, hydraulique et aéraulique ; PHY Mécanique, automatique et robotique --> PHY Electronique, automatique et robotique ; PHY Physique et état de la matière --> PHY Physique quantique, acoustique, optique, électromagnétisme ; PHY Science des matériaux--> PHY Science des matériaux et de la matière.
- Révision du périmètre des microthésaurus BIO Éthologie, BIO Neurosciences, SHS Sociologie et Psychologie.
- Révision du périmètre du microthésaurus BIO Physiologie et SHS Sociologie et Pshychologie pour les âges et classes d'âge.

**Evolution des métadonnées**
- Suppression des déclarations de propriétés et classes des modèles OWL, SKOS et SKOS-XL (inutiles pour lire le fichier)
- Nouvelle métadonnée owl:versionIRI

# V1.4 (15-09-2022)
**Chiffres globaux**
- nombre de concepts : 	15731
- nombre de termes FR : 22 142 / EN : 13 358
- nombre de définitions : 595 (155 nouvelles)
- nombre d'alignements : 3131 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Evolution du Contenu**
- Nombre de concepts créés : 31
- Nombre de concepts dépréciés (depuis le 31/05/2022) : 13
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_3052>
<http://opendata.inrae.fr/thesaurusINRAE/c_1668>
<http://opendata.inrae.fr/thesaurusINRAE/c_7863>
<http://opendata.inrae.fr/thesaurusINRAE/c_2750>
<http://opendata.inrae.fr/thesaurusINRAE/c_4003>
<http://opendata.inrae.fr/thesaurusINRAE/c_22494>
<http://opendata.inrae.fr/thesaurusINRAE/c_2715>
<http://opendata.inrae.fr/thesaurusINRAE/c_2238>
<http://opendata.inrae.fr/thesaurusINRAE/c_4082>
<http://opendata.inrae.fr/thesaurusINRAE/c_7911>
<http://opendata.inrae.fr/thesaurusINRAE/c_4006>
<http://opendata.inrae.fr/thesaurusINRAE/c_8204>
<http://opendata.inrae.fr/thesaurusINRAE/c_11288>
<http://opendata.inrae.fr/thesaurusINRAE/c_9802>

- Organisation des concepts : reclassement des concepts autour de l’élevage et des systèmes d’élevages.

# V1.3 (31-05-2022)

**Chiffres globaux**
- nombre de concepts : 	15699
- nombre de termes FR : 22 062 / EN : 13 318 
- nombre de définitions : 440
- nombre d'alignements : 3131 (GEMET, Agrovoc, MeSH, French Crop Usage)

**Nouveau Contenu**
- Nombre de concepts créés : 116 (voir https://consultation.vocabulaires-ouverts.inrae.fr/thesaurus-inrae/fr/new) 
- Nouveaux groupes : GR. SCIENCE OUVERTE (23 concepts) ; GR. BOIS ET FORÊT (468 concepts) ; GR. CONCEPTS ALIGNES ; GR. ALIGNES A AGROVOC; GR. ALIGNES A GEMET ; ALIGNES A FRENCH CROP USAGE

- Nombre de concepts dépréciés (depuis le 05/01/2022) : 32
Lorsqu'il s'agit de concepts doublons, ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_10064>
<http://opendata.inrae.fr/thesaurusINRAE/c_5673>
<http://opendata.inrae.fr/thesaurusINRAE/c_1668>
<http://opendata.inrae.fr/thesaurusINRAE/c_9391>
<http://opendata.inrae.fr/thesaurusINRAE/c_3737>
<http://opendata.inrae.fr/thesaurusINRAE/c_3122>
<http://opendata.inrae.fr/thesaurusINRAE/c_2967>
<http://opendata.inrae.fr/thesaurusINRAE/c_3480>
<http://opendata.inrae.fr/thesaurusINRAE/c_966>
<http://opendata.inrae.fr/thesaurusINRAE/c_4057>
<http://opendata.inrae.fr/thesaurusINRAE/c_13270>
<http://opendata.inrae.fr/thesaurusINRAE/c_4410>
<http://opendata.inrae.fr/thesaurusINRAE/c_14738>
<http://opendata.inrae.fr/thesaurusINRAE/c_9491>
<http://opendata.inrae.fr/thesaurusINRAE/c_9582>
<http://opendata.inrae.fr/thesaurusINRAE/c_15283>
<http://opendata.inrae.fr/thesaurusINRAE/c_9506>
<http://opendata.inrae.fr/thesaurusINRAE/c_7361>
<http://opendata.inrae.fr/thesaurusINRAE/c_17467>
<http://opendata.inrae.fr/thesaurusINRAE/c_15229>
<http://opendata.inrae.fr/thesaurusINRAE/c_9155>
<http://opendata.inrae.fr/thesaurusINRAE/c_4121>
<http://opendata.inrae.fr/thesaurusINRAE/c_2505>
<http://opendata.inrae.fr/thesaurusINRAE/c_19613>
<http://opendata.inrae.fr/thesaurusINRAE/c_15597>
<http://opendata.inrae.fr/thesaurusINRAE/c_3100>
<http://opendata.inrae.fr/thesaurusINRAE/c_15347>
<http://opendata.inrae.fr/thesaurusINRAE/c_13879>
<http://opendata.inrae.fr/thesaurusINRAE/c_6584>
<http://opendata.inrae.fr/thesaurusINRAE/c_14543>
<http://opendata.inrae.fr/thesaurusINRAE/c_2864>
<http://opendata.inrae.fr/thesaurusINRAE/c_13805>


# V1.2 (05-01-2022)

**Chiffres globaux**
- nombre de concepts : 	15566
- nombre de termes FR : 21 866 / EN : 13 142
- nombre de définitions : 294
- nombre d'alignements : 1967 (GEMET, Agrovoc, French Crop Usage)

**Nouveau Contenu**
- Nombre de concepts créés : 90 (voir https://consultation.vocabulaires-ouverts.inrae.fr/thesaurus-inrae/fr/new)
- Alignements vers French Crop Usage 
- Définitions en français

- Nombre de concepts dépréciés (depuis le 06/09/2021) : 32
Il s'agit principalement de concepts doublons. Ils sont remplacés par d'autres concepts indiqués par la propriété dcterms:isReplacedBy
- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_4435>
<http://opendata.inrae.fr/thesaurusINRAE/c_1095>
<http://opendata.inrae.fr/thesaurusINRAE/c_1554>
<http://opendata.inrae.fr/thesaurusINRAE/c_6790>
<http://opendata.inrae.fr/thesaurusINRAE/c_9230>
<http://opendata.inrae.fr/thesaurusINRAE/c_6911>
<http://opendata.inrae.fr/thesaurusINRAE/c_7525>
<http://opendata.inrae.fr/thesaurusINRAE/c_8820>
<http://opendata.inrae.fr/thesaurusINRAE/c_665>
<http://opendata.inrae.fr/thesaurusINRAE/c_7030>
<http://opendata.inrae.fr/thesaurusINRAE/c_8080>
<http://opendata.inrae.fr/thesaurusINRAE/c_6524>
<http://opendata.inrae.fr/thesaurusINRAE/c_899>
<http://opendata.inrae.fr/thesaurusINRAE/c_6863>
<http://opendata.inrae.fr/thesaurusINRAE/c_9172>
<http://opendata.inrae.fr/thesaurusINRAE/c_6312>
<http://opendata.inrae.fr/thesaurusINRAE/c_8480>
<http://opendata.inrae.fr/thesaurusINRAE/c_6117>
<http://opendata.inrae.fr/thesaurusINRAE/c_21361>
<http://opendata.inrae.fr/thesaurusINRAE/c_9155>
<http://opendata.inrae.fr/thesaurusINRAE/c_10032>
<http://opendata.inrae.fr/thesaurusINRAE/c_4006>
<http://opendata.inrae.fr/thesaurusINRAE/c_4375>
<http://opendata.inrae.fr/thesaurusINRAE/c_13901>
<http://opendata.inrae.fr/thesaurusINRAE/c_7295>
<http://opendata.inrae.fr/thesaurusINRAE/c_2864>
<http://opendata.inrae.fr/thesaurusINRAE/c_21088>
<http://opendata.inrae.fr/thesaurusINRAE/c_7490>
<http://opendata.inrae.fr/thesaurusINRAE/c_8400>
<http://opendata.inrae.fr/thesaurusINRAE/c_f7e4582e>
<http://opendata.inrae.fr/thesaurusINRAE/c_031f1e78>
<http://opendata.inrae.fr/thesaurusINRAE/c_a3deda3a>

 
- les relations inverses (topConceptOf/hasTopConcept, broader/narrower et related/related) manquantes ont été systématiquement ajoutées


# V1.1 (02-09-2021)

**Chiffres**
- nombre de concepts : 	15 476
- nombre de termes FR : 21 724 / EN : 13 006
- nombre de définitions : 33

**Schéma de données**
- skos:definition : la propriété skos:definition auparavant sous forme d'un littéral (chaine de caractères) est désormais une ressource (rdf:Resource) ayant un URI et deux propriétés. Ce choix permet de représenter de manière structurée la définition et sa source : 
    - rdf:value : contenu de la définition sous forme d’un littéral avec indication de langue
    - dcterms:source : source de la définition de type string

- dcterms:isReplacedBy : un concept déprécié (qui n'est plus utilisable, portant la propriété "owl:deprecated" avec la valeur true) présente si possible une relation dcterms:isReplacedBy qui pointe vers le concept à utiliser à sa place 

- skos:hiddenLabel : un concept peut avoir des termes cachés. Ils peuvent servir à la recherche dans un moteur mais ne doivent pas être visibles

**Contenu**
- Microthésaurus dépréciés : 
    - AGR Gestion des exploitations agricoles <http://opendata.inrae.fr/thesaurusINRAE/mt_26298>
    - TER Géographie physique <http://opendata.inrae.fr/thesaurusINRAE/mt_103>
    Note: les concepts associés ont été dispatchés dans d'autres Microthésaurus

- Concepts dépréciés : 
<http://opendata.inrae.fr/thesaurusINRAE/c_22161>, <http://opendata.inrae.fr/thesaurusINRAE/c_4435>, <http://opendata.inrae.fr/thesaurusINRAE/c_25532>, <http://opendata.inrae.fr/thesaurusINRAE/c_23410>, <http://opendata.inrae.fr/thesaurusINRAE/c_2388>, <http://opendata.inrae.fr/thesaurusINRAE/c_22171>, <http://opendata.inrae.fr/thesaurusINRAE/c_1095>, <http://opendata.inrae.fr/thesaurusINRAE/c_3140>, <http://opendata.inrae.fr/thesaurusINRAE/c_1554>, <http://opendata.inrae.fr/thesaurusINRAE/c_23875>, <http://opendata.inrae.fr/thesaurusINRAE/c_7539>, <http://opendata.inrae.fr/thesaurusINRAE/c_10269>, <http://opendata.inrae.fr/thesaurusINRAE/c_14740>, <http://opendata.inrae.fr/thesaurusINRAE/c_16376>, <http://opendata.inrae.fr/thesaurusINRAE/c_25543>, <http://opendata.inrae.fr/thesaurusINRAE/c_24201>, <http://opendata.inrae.fr/thesaurusINRAE/c_26157>, <http://opendata.inrae.fr/thesaurusINRAE/c_25057>, <http://opendata.inrae.fr/thesaurusINRAE/c_13757>, <http://opendata.inrae.fr/thesaurusINRAE/c_21965>, <http://opendata.inrae.fr/thesaurusINRAE/c_22365>, <http://opendata.inrae.fr/thesaurusINRAE/c_665>, <http://opendata.inrae.fr/thesaurusINRAE/c_8742>, <http://opendata.inrae.fr/thesaurusINRAE/c_14626>, <http://opendata.inrae.fr/thesaurusINRAE/c_23645>, <http://opendata.inrae.fr/thesaurusINRAE/c_16864>, <http://opendata.inrae.fr/thesaurusINRAE/c_24444>, <http://opendata.inrae.fr/thesaurusINRAE/c_24582>, <http://opendata.inrae.fr/thesaurusINRAE/c_7777>, <http://opendata.inrae.fr/thesaurusINRAE/c_13400>, <http://opendata.inrae.fr/thesaurusINRAE/c_4410>, <http://opendata.inrae.fr/thesaurusINRAE/c_23780>, <http://opendata.inrae.fr/thesaurusINRAE/c_25304>, <http://opendata.inrae.fr/thesaurusINRAE/c_21250>, <http://opendata.inrae.fr/thesaurusINRAE/c_22360>, <http://opendata.inrae.fr/thesaurusINRAE/c_25555>, <http://opendata.inrae.fr/thesaurusINRAE/c_11178>, <http://opendata.inrae.fr/thesaurusINRAE/c_4957>, <http://opendata.inrae.fr/thesaurusINRAE/c_25449>, <http://opendata.inrae.fr/thesaurusINRAE/c_14753>, <http://opendata.inrae.fr/thesaurusINRAE/c_24260>, <http://opendata.inrae.fr/thesaurusINRAE/c_16871>, <http://opendata.inrae.fr/thesaurusINRAE/c_14647>, <http://opendata.inrae.fr/thesaurusINRAE/c_24242>, <http://opendata.inrae.fr/thesaurusINRAE/c_20842>, <http://opendata.inrae.fr/thesaurusINRAE/c_4798>, <http://opendata.inrae.fr/thesaurusINRAE/c_23160>, <http://opendata.inrae.fr/thesaurusINRAE/c_13867>, <http://opendata.inrae.fr/thesaurusINRAE/c_15498>, <http://opendata.inrae.fr/thesaurusINRAE/c_25255>, <http://opendata.inrae.fr/thesaurusINRAE/c_21136>, <http://opendata.inrae.fr/thesaurusINRAE/c_4557>, <http://opendata.inrae.fr/thesaurusINRAE/c_2978>, <http://opendata.inrae.fr/thesaurusINRAE/c_14588>, <http://opendata.inrae.fr/thesaurusINRAE/c_11541>, <http://opendata.inrae.fr/thesaurusINRAE/c_24990>, <http://opendata.inrae.fr/thesaurusINRAE/c_13265>, <http://opendata.inrae.fr/thesaurusINRAE/c_22061>, <http://opendata.inrae.fr/thesaurusINRAE/c_7911>, <http://opendata.inrae.fr/thesaurusINRAE/c_4178>, <http://opendata.inrae.fr/thesaurusINRAE/c_17079>, <http://opendata.inrae.fr/thesaurusINRAE/c_9680>, <http://opendata.inrae.fr/thesaurusINRAE/c_24703>, <http://opendata.inrae.fr/thesaurusINRAE/c_15513>, <http://opendata.inrae.fr/thesaurusINRAE/c_26019>, <http://opendata.inrae.fr/thesaurusINRAE/c_15532>, <http://opendata.inrae.fr/thesaurusINRAE/c_13901>, <http://opendata.inrae.fr/thesaurusINRAE/c_4986>, <http://opendata.inrae.fr/thesaurusINRAE/c_5419>, <http://opendata.inrae.fr/thesaurusINRAE/c_4330>, <http://opendata.inrae.fr/thesaurusINRAE/c_07dc0ac5>
 
- Nombre de définitions ajoutées :  32
 
- les relations inverses (topConceptOf/hasTopConcept, broader/narrower et related/related) manquantes ont été systématiquement ajoutées
