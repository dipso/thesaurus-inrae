# Thesaurus INRAE

Ce répertoire est destiné à la maintenance du Thésaurus INRAE

Le Thésaurus INRAE est une ressource ouverte et partagée qui couvre les domaines de recherche d’INRAE. Il sert de référentiel au sein de l’institut pour indexer et annoter des documents, pages web, descriptions d’activités, jeux de données, à des fins de recherche ou d’analyse de l’information.

Découvrir le thésaurus INRAE : https://consultation.vocabulaires-ouverts.inrae.fr/thesaurus-inrae/fr/

Le thésaurus est administré par la DipSO et son pilotage est confié à Sophie Aubin et Magalie Weber. Un comité éditorial dédié : 

- reçoit et traite les demandes d’évolution ;

- continue les travaux de fond du type correction, ajout de définition, traduction en anglais, création de groupes ou collections pour Skosmos... ;

- mobilise des experts pour avis ou contribution ;

- assure la cohérence des microthésaurus par domaine ;

- maintient le guide du thésaurus ;

- est garant du respect des règles de rédaction et de l’adaptation du thésaurus aux besoins des utilisateurs
- participe à la conception des supports de communication, de formation

Contacter le comité éditorial : https://consultation.vocabulaires-ouverts.inrae.fr/thesaurus-inrae/fr/feedback


Le thésaurus est distribué sous licence Ouverte Etalab (équivalent CC-BY)
